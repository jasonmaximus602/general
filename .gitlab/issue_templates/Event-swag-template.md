## :package: Swag Checklist:
* [ ] swag ordered
* Price per item:
* Total $: 

**Shipping to Event:**  

* **Shipping Address:**   
* **Point of Contact:**  
* **Deadline for Shipments:**  
* **Items Ordered/Quantities/Vendor** (example: 100 notebooks from Nadel):  
* **Tracking Number:**    
* [ ] **Items Received**  

**Return Shipping:**  

* **Onsite DRI Responsible for Return Shipping:**  
* **Nadel return shipping label provided?** YES/NO  
*   **If no:**  
	* **Address for Return:**  
	* **Fedex Account Number:** 743379306  
* **Return Tracking Number** (Provided by Onsite Event DRI):  
* [ ] **Items Shipped Back**

* Mens
  
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |
  
* Women's
   
| XS | Sm | Med | Lg | XL | XXL | XXXL |
|----|----|-----|----|----|-----|------|
|    |    |     |    |    |     |      |


/label ~Events ~"Corporate Event" ~"Corporate Marketing" ~Swag
