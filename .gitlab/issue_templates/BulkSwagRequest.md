Note: we need at least 6 weeks to do new swag. We do not order net new itmes unless we can get a smaple to verify quality. This form can be used to request net new items and request existing swag reorders. This is not the place to just suggest new ideas for swag items. 

Before submitting this issue, please review the [steps for ordering new swag](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#ordering-new-swag)

## Details

**Requester:**      

**What Event:** if applicable      

**When do you need it by:** (in hand date)    

**Shipping Address and phone:**   

**Is this a new or existing item:** 

**Swag items desitred and volume:** 

**Desired spend per item:** 

**Design help needed:** will this require a new logo or somthing custom?

**Shipping Address and phone:** 


Once for submitted:
Corporate will create an item order or research request for you. You will be cced on thread with production company. Will try to close reserach and design needs within two weeks time. Then items will be ordered. 


/assign @suripatel 

/label ~"Corporate Event" ~Design ~Swag ~"mktg-status::plan"
