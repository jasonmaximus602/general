Open this to discuss toop level customer custom swag (we only do 4-5 of these a year so be sure this customer qualifies)

*To be Completed by the GitLab team hosting the giveaway:*

**BEFORE SUBMITTING THIS ISSUE:**

- [ ] Review the [GitLab Giveaway handbook page](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/)

- [ ] Provide the following basic information about your giveaway:
- Customer Name
- Desired item(s)
- Total number of people receiving the giveaway
- Attach customer logo
- Customer colors
- Add any ideas you have on what you want it to dsay or what you think they might like. 


- [ ] Complete Phase 1 of the Start a Giveaway process- we will need a finace tag to get this project going.
- [ ] Include your campaign or finance tag here `campaign or finance tag`



AFTER SUBMITTING THIS ISSUE:


*To be Completed by the fulfillment team after issue is opened:*



/assign @suripatel 

/label ~"Corporate Event" ~Design ~Swag ~"mktg-status::plan"
