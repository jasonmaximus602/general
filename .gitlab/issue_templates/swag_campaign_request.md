Thanks for hosting a swag giveaway! 

Opening this issue is Phase 4 of the Start a Giveaway process. 

**If you haven't yet reviewed the handbook AND completed Phase 1-3**, please read the[GitLab Giveaway handbook page](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/giveaways/)

*To be Completed by the GitLab team hosting the giveaway:*

**BEFORE SUBMITTING THIS ISSUE:**

- [ ] Review the [GitLab Giveaway handbook page](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/giveaways/)

- [ ] Provide the following basic information about your giveaway:
- Your team name
- Desired date to start giveaway
- Total number of people receiving the giveaway

- [ ] Complete Phase 1 of the Start a Giveaway process
- [ ] Include your campaign or finance tag here `campaign or finance tag`

- [ ] Complete Phase 2 of the Start a Giveaway process
- [ ] Source your swag.
- [ ] I plan on using `custom`, `existing` or `bulk` swag for this giveaway

* If you plan to use custom swag, please link your design request issues below and include images of the swag in this issue:

> **Do you plan to use existing swag inventory that is currently in the Printfection warehouse for this giveaway?**
> 
> * [ ] Yes
> * [ ] No
> 
> If you plan to use our existing swag, please note what we have currently available in the store:
> 
> For giveaways with <100 winners
> 
> * Socks
> * Any items available for giveaways with >100 winners
> * Items you ordered specifically for this giveaway
> 
> For giveaways with >100 winners
> 
> * Stickers
> * T-Shirts
>
> If you **do not** plan to use existing inventory and need to order swag, please review the process for [ordering new swag](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/#ordering-new-swag). Add a comment to this issue if you need support from the corporate team in sourcing swag or brainstorming ideas.

- [ ] Complete Phase 3 of the Start a Giveaway Process
- [ ] My team will `send unique giveaway links` or `create a multi-use giveaway link`
- [ ] My team will `generate a bulk list of links` or `use the printfection chrome extension`


AFTER SUBMITTING THIS ISSUE:


*To be Completed by the fulfillment team after issue is opened:*

If ordering custom swag:

- [ ] Share printfection warehouse address
- [ ] Create a new item in Printfection for all new swag
- [ ] Create a customer sourced send order after custom swag is shipped, including tracking information


If using bulk swag:

- [ ] Review total inventory of desired bulk swag to confirm there is enough for their giveaway

For all giveaways:
- [ ] Create giveaway campaign
- [ ] If giving away multiple items, create giveaway kits
- [ ] Review preferred process for generating and sending giveaway links


/assign @suripatel

/label ~"Corporate Event" ~Design ~Swag ~"mktg-status::plan"
